class CalendarController < ApplicationController
 before_filter :authenticate_user!

  # カレンダーを表示
  def show
    @month = (params[:month] || (Time.zone || Time).now.month).to_i
    @year = (params[:year] || (Time.zone || Time).now.year).to_i
    @shown_month = Date.civil(@year, @month)

    # ログインユーザーで絞り込む
    @event_strips = Event.event_strips_for_month(
      @shown_month,
      0,
      :conditions => { :user_id => current_user.id })
  end
  
end