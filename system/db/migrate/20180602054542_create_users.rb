class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|

      t.string :name
      t.integer :age
      t.date :enroll
      t.string :license

      t.timestamps null: false
    end
  end
end
