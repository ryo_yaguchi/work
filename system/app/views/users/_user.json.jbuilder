json.extract! user, :id, :name, :age, :enroll, :license, :skill_1, :created_at, :updated_at
json.url user_url(user, format: :json)
